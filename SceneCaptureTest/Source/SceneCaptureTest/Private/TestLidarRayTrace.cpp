// Fill out your copyright notice in the Description page of Project Settings.

#include "TestLidarRayTrace.h"

#include "Components/InstancedStaticMeshComponent.h"

DECLARE_STATS_GROUP(TEXT("Lidar"), STATGROUP_Lidar, STATCAT_Advanced);
DECLARE_CYCLE_STAT(TEXT("Lidar Raytrace"), STAT_LidarRayTrace, STATGROUP_Lidar);

// Sets default values for this component's properties
UTestLidarRayTrace::UTestLidarRayTrace()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	bTickInEditor = false;
	
	//PrimaryComponentTick.bRunOnAnyThread = true;
	//PrimaryComponentTick.TickGroup = TG_PrePhysics;
	//PrimaryComponentTick.EndTickGroup = TG_PrePhysics;
	SetTickGroup(TG_PrePhysics);

	// ...
	InstancedMeshComponent = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("LidarPoints"));

}


// Called when the game starts
void UTestLidarRayTrace::BeginPlay()
{
	Super::BeginPlay();

	// ...
	InitializeLidar();

	InstancedMeshComponent->SetStaticMesh(DrawPointMesh);
	InstancedMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	InstancedMeshComponent->SetSimulatePhysics(false);
	
}


void UTestLidarRayTrace::InitializeLidar()
{
	/// Initialize our elevation angles
	float MinElevRad = FMath::DegreesToRadians(MinElevationAngle);
	float MaxElevRad = FMath::DegreesToRadians(MaxElevationAngle);
	float DiffElev = (MaxElevRad - MinElevRad) / (NumberBeams - 1);

	if (ScanRateHz > 1.0)
	{
		ScanDeltaTime = 1.0f / ScanRateHz;
		ScanTimeAccumulator = 0.0f;
	}

	TArray<float> CosElev;
	CosElev.SetNum(NumberBeams);
	TArray<float> SinElev;
	SinElev.SetNum(NumberBeams);
	float ElevRad = MinElevRad;
	for (uint32 i = 0; i < NumberBeams; ++i)
	{
		CosElev[i] = FMath::Cos(ElevRad);
		SinElev[i] = FMath::Sin(ElevRad);
		ElevRad += DiffElev;
	}

	uint32 NumberAzimuth = FMath::Clamp<uint32>(360.0 / AzimuthAngleResolution, 1, 20000) + 1;

	uint32 TotalBeamHits = NumberBeams * NumberAzimuth;
	UE_LOG(LogTemp, Warning, TEXT("Setting up Lidar with %i points per trace"), TotalBeamHits);
	
	BeamVectors.SetNum(TotalBeamHits);
	HitRanges.SetNum(TotalBeamHits);
	HitPoints.SetNum(TotalBeamHits);
	WorldPoints.SetNum(TotalBeamHits);

	float Azimuth = 0.0;
	const float DiffAzimuth = FMath::DegreesToRadians(AzimuthAngleResolution);
	uint32 BeamIndex = 0;
	for (uint32 Az = 0; Az < NumberAzimuth; ++Az)
	{
		float CosAz = FMath::Cos(Azimuth);
		float SinAz = FMath::Sin(Azimuth);

		for (int32 Elev = 0; Elev < CosElev.Num(); ++Elev)
		{
			FVector& Beam = BeamVectors[BeamIndex];
			Beam.X = MaximumRange * CosAz * CosElev[Elev];
			Beam.Y = MaximumRange * SinAz * CosElev[Elev];
			Beam.Z = MaximumRange * SinElev[Elev];

			++BeamIndex;
		}

		Azimuth += DiffAzimuth;
	}
}

void UTestLidarRayTrace::RayTraceLidarBeams()
{
	SCOPE_CYCLE_COUNTER(STAT_LidarRayTrace);

	FVector BeamStart = GetComponentTransform().GetTranslation();
	FRotator LidarRotation = GetComponentTransform().Rotator();

	FCollisionObjectQueryParams QueryParams(
			ECC_TO_BITFIELD(ECC_WorldStatic)
		|	ECC_TO_BITFIELD(ECC_WorldDynamic)
		|	ECC_TO_BITFIELD(ECC_Pawn)
		|   ECC_TO_BITFIELD(ECC_PhysicsBody)
	);
	FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());
	
	FHitResult Hit;
	for (int32 Beam = 0; Beam < BeamVectors.Num(); ++Beam)
	{
		// Apply lidar rotation to end point
		FVector BeamEnd = BeamStart + LidarRotation.RotateVector(BeamVectors[Beam]);
		// Apply the line trace
		GetWorld()->LineTraceSingleByObjectType(Hit, BeamStart, BeamEnd,
			QueryParams, TraceParams);

		if (Hit.IsValidBlockingHit())
		{
			HitRanges[Beam] = Hit.Distance;
			// Convert this to local vehicle frame
			HitPoints[Beam] = LidarRotation.UnrotateVector( Hit.Location - BeamStart);
			WorldPoints[Beam] = Hit.Location;
			
		}
		else
		{
			HitRanges[Beam] = -1.0f;
			HitPoints[Beam] = FVector(0.0, 0.0, 0.0);
			WorldPoints[Beam] = FVector(0.0, 0.0, 0.0);
		}
	}

}

void UTestLidarRayTrace::DrawPoints()
{
	if (DrawPointMesh == nullptr)
		return;

	InstancedMeshComponent->ClearInstances();

	int32 count = 0;
	FTransform WorldTransform;
	WorldTransform.SetScale3D(FVector(DrawPointScale));
	for (int32 i = 0; i < HitRanges.Num(); ++i)
	{
		if (HitRanges[i] > 0.0)
		{
			WorldTransform.SetLocation(WorldPoints[i]);

			InstancedMeshComponent->AddInstanceWorldSpace(WorldTransform);

			++count;
		}
	}

	//UE_LOG(LogTemp, Warning, TEXT("Drawing %i Instanced Points"), count)
}

// Called every frame
void UTestLidarRayTrace::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!bEnableSensor)
		return;

	ScanTimeAccumulator += DeltaTime;
	if (ScanTimeAccumulator > ScanDeltaTime)
	{
		RayTraceLidarBeams();

		if (bDrawPoints)
		{
			DrawPoints();
		}
		ScanTimeAccumulator -= ScanDeltaTime;
	}
}

