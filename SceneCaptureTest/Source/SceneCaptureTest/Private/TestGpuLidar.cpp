// Fill out your copyright notice in the Description page of Project Settings.

#include "TestGpuLidar.h"

#include "RHICommandList.h"

#include "ReadbackSceneCaptureComponent2D.h"


// Sets default values for this component's properties
UTestGpuLidar::UTestGpuLidar()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bTickInEditor = false;

	// ...
}

void UTestGpuLidar::CaptureScene(uint32 CameraIndex)
{
	if (CameraIndex < NumCameras)
	{
		DepthCameraArray[CameraIndex]->CaptureScene();
	}
}

void UTestGpuLidar::CaptureSceneDeferred(uint32 CameraIndex)
{
	if (CameraIndex < NumCameras)
	{
		DepthCameraArray[CameraIndex]->CaptureSceneDeferred();
	}
}


// Called when the game starts
void UTestGpuLidar::BeginPlay()
{
	Super::BeginPlay();

	InitializeCameraArray();
}


void UTestGpuLidar::InitializeCameraArray()
{
	float CameraYaw = 0.0f;
	float DiffYaw = 90.0f;

	// FIXME this isn't goign to work for non 360 active view
	CameraFireInterval = (1.0f / ScanRate); // / NumCameras;

	const uint32 ImageWidth = FMath::RoundUpToPowerOfTwo(DiffYaw / AzimuthAngleResolution);
	// FIXME Might want to discard these
	const uint32 ImageHeight = FMath::RoundUpToPowerOfTwo(NumberBeams);

	UE_LOG(LogTemp, Warning, TEXT("Initializing Depth Cameras with Resolution %i x %i"), ImageWidth, ImageHeight)

		// Compute this camera's view matrix
		float HorizFov = FMath::DegreesToRadians(DiffYaw);
	// FIXME deal with asymetric elevation angles (+10 to -30, for instance)
	const float VertFov = FMath::DegreesToRadians(MaxElevationAngle - MinElevationAngle);
	const float NearPlane = 200.0f;
	const float FarPlane = MaximumRange;
	const float W = 1.0f / FMath::Tan(HorizFov*0.5f);
	const float H = 1.0f / FMath::Tan(VertFov*0.5f);
	const float Q = ((NearPlane == FarPlane) ? (1.0f - Z_PRECISION) : FarPlane / (FarPlane - NearPlane));
	const float R = -NearPlane * Q;
	FMatrix ProjMatrix(
		FPlane(W, 0.0f, 0.0f, 0.0f),
		FPlane(0.0f, H, 0.0f, 0.0f),
		FPlane(0.0f, 0.0f, Q, 1.0f),
		FPlane(0.0f, 0.0f, R, 0.0f));


	DepthCameraArray.SetNum(NumCameras);
	for (uint32 i = 0; i < NumCameras; ++i)
	{
		// Create the capture component
		UReadbackSceneCaptureComponent2D* DepthCamera = NewObject<UReadbackSceneCaptureComponent2D>(this);
		DepthCamera->bCaptureEveryFrame = false;
		DepthCamera->bCaptureOnMovement = false;
		DepthCamera->bUseCustomProjectionMatrix = true;
		/// Could use scene color + scene depth for intensity reading
		DepthCamera->CaptureSource = SCS_SceneDepth;
		DepthCamera->CustomProjectionMatrix = ProjMatrix;

		DepthCamera->Initialize(ImageWidth, ImageHeight, PF_B8G8R8A8, 1);

		DepthCameraArray[i] = DepthCamera;
		CameraYaw += DiffYaw;
	}
}


// Called every frame
void UTestGpuLidar::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!bEnableSensor)
	{
		return;
	}

	CameraFireAccumulator += DeltaTime;
	while (CameraFireAccumulator > CameraFireInterval)
	{
		CameraFireAccumulator -= CameraFireInterval;
		
		//UE_LOG(LogTemp, Warning, TEXT("Firing Depth Camera %i"), NextCameraToFire)
		for (uint32 i = 0; i < NumCameras; ++i)
		{
			//CaptureScene(i);
			CaptureSceneDeferred(i);

			ReadbackRangePixels(i);
		}
		//CaptureSceneDeferred(NextCameraToFire);

		NextCameraToFire = (NextCameraToFire + 1) % NumCameras;
	}
}

void UTestGpuLidar::ReadbackRangePixels(uint32 CameraIndex)
{
	if (CameraIndex < NumCameras)
	{
		DepthCameraArray[CameraIndex]->RequestRead();
	}
}