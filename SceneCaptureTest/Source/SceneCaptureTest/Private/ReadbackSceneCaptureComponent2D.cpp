// Fill out your copyright notice in the Description page of Project Settings.

#include "ReadbackSceneCaptureComponent2D.h"

#include "EngineModule.h"
#include "RHICommandList.h"
#include "Engine/TextureRenderTarget2D.h"


UReadbackSceneCaptureComponent2D::FCaptureHeader::FCaptureHeader()
	: SourceFrameNumber(0)
{
	;
}

UReadbackSceneCaptureComponent2D::FCaptureData::FCaptureData()
	: Data(nullptr), Width(0), Height(0), PixelDepth(0)
{
	;
}

UReadbackSceneCaptureComponent2D::FCaptureData::~FCaptureData()
{
	delete[] Data;
}

UReadbackSceneCaptureComponent2D::FCaptureFrame::FCaptureFrame()
	: bResolvedTargetRequested(false)
{
	;
}

UReadbackSceneCaptureComponent2D::UReadbackSceneCaptureComponent2D()
{
	;
}


bool UReadbackSceneCaptureComponent2D::Initialize(int32 Width, int32 Height, EPixelFormat Format, int32 BufferSize)
{
	NumberOfCaptureFrames = BufferSize;
	ImageWidth = Width;
	ImageHeight = Height;
	PixelFormat = Format;

	// Create our texture target
	TextureTarget = NewObject<UTextureRenderTarget2D>(this);
	TextureTarget->CompressionSettings = TextureCompressionSettings::TC_Default;
	TextureTarget->SRGB = false;
	TextureTarget->bAutoGenerateMips = false;
	TextureTarget->AddressX = TextureAddress::TA_Clamp;
	TextureTarget->AddressY = TextureAddress::TA_Clamp;
	TextureTarget->bGPUSharedFlag = true;

	// Initialize texture target
	TextureTarget->InitCustomFormat(ImageWidth, ImageHeight, PixelFormat, true);
	TextureTarget->UpdateResourceImmediate();

	uint32 PixelDepth = 0;
	switch (Format)
	{
	case PF_B8G8R8A8:
	case PF_R32_FLOAT:
		PixelDepth = 4u;
		break;
	default:
		UE_LOG(LogTemp, Error, TEXT("Unsupported Pixel Format %i"), Format);
		return false;
	};

	// Allocate our return buffer
	CaptureData.Data = new uint8[ImageWidth * ImageHeight * PixelDepth];
	if (CaptureData.Data == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Could not allocate CaptureFraem Buffer"))
		return false;
	}
	CaptureData.Width = ImageWidth;
	CaptureData.Height = ImageHeight;
	CaptureData.PixelDepth = PixelDepth;

	// Enqueue our readback buffer allocation
	CaptureBuffer.SetNum(NumberOfCaptureFrames);
	CurrentResolvedTargetIndex = 0;
	auto RenderCommand = [this](FRHICommandListImmediate& RHICmdList)
	{
		FRHIResourceCreateInfo CreateInfo;
		for (int32 Index = 0; Index < NumberOfCaptureFrames; ++Index)
		{
			CaptureBuffer[Index].ReadbackTexture =
				RHICreateTexture2D(ImageWidth, ImageHeight, PixelFormat,
					1, 1, TexCreate_CPUReadback, CreateInfo);
		}
		bResolvedTargetInitialized = true;
	};

	// Enqueue our allocation command
	ENQUEUE_UNIQUE_RENDER_COMMAND_ONEPARAMETER(
		ReadbackTextureAllocate,
		decltype(RenderCommand), InRenderCommand, RenderCommand,
		{
			InRenderCommand(RHICmdList);
		}
	);
	
	return true;
}

void UReadbackSceneCaptureComponent2D::BeginPlay()
{
	Super::BeginPlay();

	SetDefaultOverrides();
	RemoveShowFlags();
}

void UReadbackSceneCaptureComponent2D::RequestRead()
{
	// Make sure our buffer initialization has finished
	if (!bResolvedTargetInitialized)
	{
		FlushRenderingCommands();
	}

	CurrentReadyFrameIndex = (CurrentResolvedTargetIndex) % NumberOfCaptureFrames;
	CurrentResolvedTargetIndex = (CurrentResolvedTargetIndex + 1) % NumberOfCaptureFrames;

	// Frames are only ready if they're resolved
	FCaptureFrame* ReadyFrame = &CaptureBuffer[CurrentReadyFrameIndex];
	FCaptureFrame* CaptureFrame = &CaptureBuffer[CurrentResolvedTargetIndex];

	bWaitingForResolveCommandExecution = true;

	auto RenderCommand = [this](FRHICommandListImmediate& RHICmdList,
		FCaptureFrame* InCapturingFrame, FCaptureFrame* InReadyFrame)
	{
		FTexture2DRHIRef SourceTexture;
		{
			FScopeLock Lock(&AccessingRenderTarget);
			UTextureRenderTarget2D* InCaptureRenderTarget = TextureTarget;
			SourceTexture = InCaptureRenderTarget->GetRenderTargetResource()
				->GetTextureRenderTarget2DResource()->GetTextureRHI();
		}

		// Queue Copying from the Source to the current capture buffer
		if (InCapturingFrame)
		{
			// Copy from GPU to CPU
			RHICmdList.CopyToResolveTarget(SourceTexture,
				InCapturingFrame->ReadbackTexture, FResolveParams());
		
			InCapturingFrame->bResolvedTargetRequested = true;
		}

		if (InReadyFrame)
		{
			// Lock and read the data
			void* ReadbackData = nullptr;
			int32 Width = 0;
			int32 Height = 0;
			
			RHICmdList.MapStagingSurface(InReadyFrame->ReadbackTexture, ReadbackData, Width, Height);

			// Copy to our data buffer
			FMemory::BigBlockMemcpy(CaptureData.Data, ReadbackData, Width*Height*CaptureData.PixelDepth);

			RHICmdList.UnmapStagingSurface(InReadyFrame->ReadbackTexture);
			
		}

		bWaitingForResolveCommandExecution = false;
	};

	ENQUEUE_UNIQUE_RENDER_COMMAND_THREEPARAMETER(
		ReadbackTextureCopy,
		FCaptureFrame*, InCaptureFrame, CaptureFrame,
		FCaptureFrame*, InPreviousFrame, ReadyFrame,
		decltype(RenderCommand), InRenderCommand, RenderCommand,
		{
			InRenderCommand(RHICmdList, InCaptureFrame, InPreviousFrame);
		}
	);
}

bool UReadbackSceneCaptureComponent2D::DataReady() const
{
	// Is this sufficinet?
	return !bWaitingForResolveCommandExecution; 
}

void UReadbackSceneCaptureComponent2D::SetDefaultOverrides()
{
	auto& Settings = this->PostProcessSettings;
	Settings.bOverride_AutoExposureMethod = true;
	Settings.AutoExposureMethod = AEM_Histogram;
	Settings.bOverride_AutoExposureMinBrightness = true;
	Settings.AutoExposureMinBrightness = 0.27f;
	Settings.bOverride_AutoExposureMaxBrightness = true;
	Settings.AutoExposureMaxBrightness = 5.0f;
	Settings.bOverride_AutoExposureBias = true;
	Settings.AutoExposureBias = -3.5f;
}

void UReadbackSceneCaptureComponent2D::RemoveShowFlags()
{
	auto& Flags = this->ShowFlags;
	Flags.SetAmbientOcclusion(false);
	Flags.SetAntiAliasing(false);
	Flags.SetAtmosphericFog(false);
	// Flags.SetAudioRadius(false);
	// Flags.SetBillboardSprites(false);
	Flags.SetBloom(false);
	// Flags.SetBounds(false);
	// Flags.SetBrushes(false);
	// Flags.SetBSP(false);
	// Flags.SetBSPSplit(false);
	// Flags.SetBSPTriangles(false);
	// Flags.SetBuilderBrush(false);
	// Flags.SetCameraAspectRatioBars(false);
	// Flags.SetCameraFrustums(false);
	Flags.SetCameraImperfections(false);
	Flags.SetCameraInterpolation(false);
	// Flags.SetCameraSafeFrames(false);
	// Flags.SetCollision(false);
	// Flags.SetCollisionPawn(false);
	// Flags.SetCollisionVisibility(false);
	Flags.SetColorGrading(false);
	// Flags.SetCompositeEditorPrimitives(false);
	// Flags.SetConstraints(false);
	// Flags.SetCover(false);
	// Flags.SetDebugAI(false);
	// Flags.SetDecals(false);
	// Flags.SetDeferredLighting(false);
	Flags.SetDepthOfField(false);
	Flags.SetDiffuse(false);
	Flags.SetDirectionalLights(false);
	Flags.SetDirectLighting(false);
	// Flags.SetDistanceCulledPrimitives(false);
	// Flags.SetDistanceFieldAO(false);
	// Flags.SetDistanceFieldGI(false);
	Flags.SetDynamicShadows(false);
	// Flags.SetEditor(false);
	Flags.SetEyeAdaptation(false);
	Flags.SetFog(false);
	// Flags.SetGame(false);
	// Flags.SetGameplayDebug(false);
	// Flags.SetGBufferHints(false);
	Flags.SetGlobalIllumination(false);
	Flags.SetGrain(false);
	// Flags.SetGrid(false);
	// Flags.SetHighResScreenshotMask(false);
	// Flags.SetHitProxies(false);
	Flags.SetHLODColoration(false);
	Flags.SetHMDDistortion(false);
	// Flags.SetIndirectLightingCache(false);
	// Flags.SetInstancedFoliage(false);
	// Flags.SetInstancedGrass(false);
	// Flags.SetInstancedStaticMeshes(false);
	// Flags.SetLandscape(false);
	// Flags.SetLargeVertices(false);
	Flags.SetLensFlares(false);
	Flags.SetLevelColoration(false);
	Flags.SetLightComplexity(false);
	Flags.SetLightFunctions(false);
	Flags.SetLightInfluences(false);
	Flags.SetLighting(false);
	Flags.SetLightMapDensity(false);
	Flags.SetLightRadius(false);
	Flags.SetLightShafts(false);
	// Flags.SetLOD(false);
	Flags.SetLODColoration(false);
	// Flags.SetMaterials(false);
	// Flags.SetMaterialTextureScaleAccuracy(false);
	// Flags.SetMeshEdges(false);
	// Flags.SetMeshUVDensityAccuracy(false);
	// Flags.SetModeWidgets(false);
	Flags.SetMotionBlur(false);
	// Flags.SetNavigation(false);
	Flags.SetOnScreenDebug(false);
	// Flags.SetOutputMaterialTextureScales(false);
	// Flags.SetOverrideDiffuseAndSpecular(false);
	// Flags.SetPaper2DSprites(false);
	Flags.SetParticles(false);
	// Flags.SetPivot(false);
	Flags.SetPointLights(false);
	// Flags.SetPostProcessing(false);
	// Flags.SetPostProcessMaterial(false);
	// Flags.SetPrecomputedVisibility(false);
	// Flags.SetPrecomputedVisibilityCells(false);
	// Flags.SetPreviewShadowsIndicator(false);
	// Flags.SetPrimitiveDistanceAccuracy(false);
	Flags.SetPropertyColoration(false);
	// Flags.SetQuadOverdraw(false);
	// Flags.SetReflectionEnvironment(false);
	// Flags.SetReflectionOverride(false);
	Flags.SetRefraction(false);
	// Flags.SetRendering(false);
	Flags.SetSceneColorFringe(false);
	// Flags.SetScreenPercentage(false);
	Flags.SetScreenSpaceAO(false);
	Flags.SetScreenSpaceReflections(false);
	// Flags.SetSelection(false);
	// Flags.SetSelectionOutline(false);
	// Flags.SetSeparateTranslucency(false);
	// Flags.SetShaderComplexity(false);
	// Flags.SetShaderComplexityWithQuadOverdraw(false);
	// Flags.SetShadowFrustums(false);
	// Flags.SetSkeletalMeshes(false);
	// Flags.SetSkinCache(false);
	Flags.SetSkyLighting(false);
	// Flags.SetSnap(false);
	// Flags.SetSpecular(false);
	// Flags.SetSplines(false);
	Flags.SetSpotLights(false);
	// Flags.SetStaticMeshes(false);
	Flags.SetStationaryLightOverlap(false);
	// Flags.SetStereoRendering(false);
	// Flags.SetStreamingBounds(false);
	Flags.SetSubsurfaceScattering(false);
	// Flags.SetTemporalAA(false);
	// Flags.SetTessellation(false);
	// Flags.SetTestImage(false);
	// Flags.SetTextRender(false);
	// Flags.SetTexturedLightProfiles(false);
	Flags.SetTonemapper(false);
	// Flags.SetTranslucency(false);
	// Flags.SetVectorFields(false);
	// Flags.SetVertexColors(false);
	// Flags.SetVignette(false);
	// Flags.SetVisLog(false);
	Flags.SetVisualizeAdaptiveDOF(false);
	Flags.SetVisualizeBloom(false);
	Flags.SetVisualizeBuffer(false);
	Flags.SetVisualizeDistanceFieldAO(false);
	Flags.SetVisualizeDistanceFieldGI(false);
	Flags.SetVisualizeDOF(false);
	Flags.SetVisualizeHDR(false);
	Flags.SetVisualizeLightCulling(false);
	Flags.SetVisualizeLPV(false);
	Flags.SetVisualizeMeshDistanceFields(false);
	Flags.SetVisualizeMotionBlur(false);
	Flags.SetVisualizeOutOfBoundsPixels(false);
	Flags.SetVisualizeSenses(false);
	Flags.SetVisualizeShadingModels(false);
	Flags.SetVisualizeSSR(false);
	Flags.SetVisualizeSSS(false);
	// Flags.SetVolumeLightingSamples(false);
	// Flags.SetVolumes(false);
	// Flags.SetWidgetComponents(false);
	// Flags.SetWireframe(false);
}