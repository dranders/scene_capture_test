// Fill out your copyright notice in the Description page of Project Settings.

#include "TestCaptureCamera.h"

#include "EngineModule.h"
#include "RHICommandList.h"
#include "Engine/TextureRenderTarget2D.h"

UTestCaptureCamera::UTestCaptureCamera()
{
  PrimaryComponentTick.bCanEverTick = true;
  bTickInEditor = false;

  bCaptureEveryFrame = false;
}

void UTestCaptureCamera::BeginPlay()
{
  Super::BeginPlay();

  if (FrameRate > 1.0)
  {
    CameraDeltaTime = 1.0 / FrameRate;
  }

  CacheBuffer.SetNum(4u * ImageWidth * ImageHeight);
  
  SetDefaultOverrides();
  RemoveShowFlags();

  // Set up our texture target
  Initialize(2);
}

void UTestCaptureCamera::Initialize(uint32 NumBuffers)
{
  // Create and setup our texture target
  TextureTarget = NewObject<UTextureRenderTarget2D>(this);
  TextureTarget->CompressionSettings = TextureCompressionSettings::TC_Default;
  TextureTarget->SRGB = false;
  TextureTarget->bAutoGenerateMips = false;
  TextureTarget->bGPUSharedFlag = true;
  TextureTarget->AddressX = TextureAddress::TA_Clamp;
  TextureTarget->AddressY = TextureAddress::TA_Clamp;

  TextureTarget->InitCustomFormat(ImageWidth, ImageHeight, PixelFormat, true);
  //TextureTarget->InitAutoFormat(ImageWidth, ImageHeight);

}


void UTestCaptureCamera::TickComponent(float DeltaTime, ELevelTick TickType,
  FActorComponentTickFunction* ThisTickFunction)
{
  Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

  if (!bEnableCapture)
  {
    return;
  }

  CameraTimeAccumulator += DeltaTime;
  if (CameraTimeAccumulator > CameraDeltaTime)
  {
    CameraTimeAccumulator -= CameraDeltaTime;

    CaptureSceneDeferred();
	//CaptureScene();

    if (bEnableReadback)
    {
      ReadBackTexture();
    }
  }
}

void UTestCaptureCamera::ReadBackTexture()
{
  ;
}


void UTestCaptureCamera::SetDefaultOverrides()
{
	auto& Settings = this->PostProcessSettings;
	Settings.bOverride_AutoExposureMethod = true;
	Settings.AutoExposureMethod = AEM_Histogram;
	Settings.bOverride_AutoExposureMinBrightness = true;
	Settings.AutoExposureMinBrightness = 0.27f;
	Settings.bOverride_AutoExposureMaxBrightness = true;
	Settings.AutoExposureMaxBrightness = 5.0f;
	Settings.bOverride_AutoExposureBias = true;
	Settings.AutoExposureBias = -3.5f;
}

void UTestCaptureCamera::RemoveShowFlags()
{
	auto& Flags = this->ShowFlags;
	Flags.SetAmbientOcclusion(false);
	Flags.SetAntiAliasing(false);
	Flags.SetAtmosphericFog(false);
	// Flags.SetAudioRadius(false);
	// Flags.SetBillboardSprites(false);
	Flags.SetBloom(false);
	// Flags.SetBounds(false);
	// Flags.SetBrushes(false);
	// Flags.SetBSP(false);
	// Flags.SetBSPSplit(false);
	// Flags.SetBSPTriangles(false);
	// Flags.SetBuilderBrush(false);
	// Flags.SetCameraAspectRatioBars(false);
	// Flags.SetCameraFrustums(false);
	Flags.SetCameraImperfections(false);
	Flags.SetCameraInterpolation(false);
	// Flags.SetCameraSafeFrames(false);
	// Flags.SetCollision(false);
	// Flags.SetCollisionPawn(false);
	// Flags.SetCollisionVisibility(false);
	Flags.SetColorGrading(false);
	// Flags.SetCompositeEditorPrimitives(false);
	// Flags.SetConstraints(false);
	// Flags.SetCover(false);
	// Flags.SetDebugAI(false);
	// Flags.SetDecals(false);
	// Flags.SetDeferredLighting(false);
	Flags.SetDepthOfField(false);
	Flags.SetDiffuse(false);
	Flags.SetDirectionalLights(false);
	Flags.SetDirectLighting(false);
	// Flags.SetDistanceCulledPrimitives(false);
	// Flags.SetDistanceFieldAO(false);
	// Flags.SetDistanceFieldGI(false);
	Flags.SetDynamicShadows(false);
	// Flags.SetEditor(false);
	Flags.SetEyeAdaptation(false);
	Flags.SetFog(false);
	// Flags.SetGame(false);
	// Flags.SetGameplayDebug(false);
	// Flags.SetGBufferHints(false);
	Flags.SetGlobalIllumination(false);
	Flags.SetGrain(false);
	// Flags.SetGrid(false);
	// Flags.SetHighResScreenshotMask(false);
	// Flags.SetHitProxies(false);
	Flags.SetHLODColoration(false);
	Flags.SetHMDDistortion(false);
	// Flags.SetIndirectLightingCache(false);
	// Flags.SetInstancedFoliage(false);
	// Flags.SetInstancedGrass(false);
	// Flags.SetInstancedStaticMeshes(false);
	// Flags.SetLandscape(false);
	// Flags.SetLargeVertices(false);
	Flags.SetLensFlares(false);
	Flags.SetLevelColoration(false);
	Flags.SetLightComplexity(false);
	Flags.SetLightFunctions(false);
	Flags.SetLightInfluences(false);
	Flags.SetLighting(false);
	Flags.SetLightMapDensity(false);
	Flags.SetLightRadius(false);
	Flags.SetLightShafts(false);
	// Flags.SetLOD(false);
	Flags.SetLODColoration(false);
	// Flags.SetMaterials(false);
	// Flags.SetMaterialTextureScaleAccuracy(false);
	// Flags.SetMeshEdges(false);
	// Flags.SetMeshUVDensityAccuracy(false);
	// Flags.SetModeWidgets(false);
	Flags.SetMotionBlur(false);
	// Flags.SetNavigation(false);
	Flags.SetOnScreenDebug(false);
	// Flags.SetOutputMaterialTextureScales(false);
	// Flags.SetOverrideDiffuseAndSpecular(false);
	// Flags.SetPaper2DSprites(false);
	Flags.SetParticles(false);
	// Flags.SetPivot(false);
	Flags.SetPointLights(false);
	// Flags.SetPostProcessing(false);
	// Flags.SetPostProcessMaterial(false);
	// Flags.SetPrecomputedVisibility(false);
	// Flags.SetPrecomputedVisibilityCells(false);
	// Flags.SetPreviewShadowsIndicator(false);
	// Flags.SetPrimitiveDistanceAccuracy(false);
	Flags.SetPropertyColoration(false);
	// Flags.SetQuadOverdraw(false);
	// Flags.SetReflectionEnvironment(false);
	// Flags.SetReflectionOverride(false);
	Flags.SetRefraction(false);
	// Flags.SetRendering(false);
	Flags.SetSceneColorFringe(false);
	// Flags.SetScreenPercentage(false);
	Flags.SetScreenSpaceAO(false);
	Flags.SetScreenSpaceReflections(false);
	// Flags.SetSelection(false);
	// Flags.SetSelectionOutline(false);
	// Flags.SetSeparateTranslucency(false);
	// Flags.SetShaderComplexity(false);
	// Flags.SetShaderComplexityWithQuadOverdraw(false);
	// Flags.SetShadowFrustums(false);
	// Flags.SetSkeletalMeshes(false);
	// Flags.SetSkinCache(false);
	Flags.SetSkyLighting(false);
	// Flags.SetSnap(false);
	// Flags.SetSpecular(false);
	// Flags.SetSplines(false);
	Flags.SetSpotLights(false);
	// Flags.SetStaticMeshes(false);
	Flags.SetStationaryLightOverlap(false);
	// Flags.SetStereoRendering(false);
	// Flags.SetStreamingBounds(false);
	Flags.SetSubsurfaceScattering(false);
	// Flags.SetTemporalAA(false);
	// Flags.SetTessellation(false);
	// Flags.SetTestImage(false);
	// Flags.SetTextRender(false);
	// Flags.SetTexturedLightProfiles(false);
	Flags.SetTonemapper(false);
	// Flags.SetTranslucency(false);
	// Flags.SetVectorFields(false);
	// Flags.SetVertexColors(false);
	// Flags.SetVignette(false);
	// Flags.SetVisLog(false);
	Flags.SetVisualizeAdaptiveDOF(false);
	Flags.SetVisualizeBloom(false);
	Flags.SetVisualizeBuffer(false);
	Flags.SetVisualizeDistanceFieldAO(false);
	Flags.SetVisualizeDistanceFieldGI(false);
	Flags.SetVisualizeDOF(false);
	Flags.SetVisualizeHDR(false);
	Flags.SetVisualizeLightCulling(false);
	Flags.SetVisualizeLPV(false);
	Flags.SetVisualizeMeshDistanceFields(false);
	Flags.SetVisualizeMotionBlur(false);
	Flags.SetVisualizeOutOfBoundsPixels(false);
	Flags.SetVisualizeSenses(false);
	Flags.SetVisualizeShadingModels(false);
	Flags.SetVisualizeSSR(false);
	Flags.SetVisualizeSSS(false);
	// Flags.SetVolumeLightingSamples(false);
	// Flags.SetVolumes(false);
	// Flags.SetWidgetComponents(false);
	// Flags.SetWireframe(false);
}