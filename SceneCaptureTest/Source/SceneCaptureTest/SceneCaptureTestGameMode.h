// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "SceneCaptureTestGameMode.generated.h"

UCLASS(minimalapi)
class ASceneCaptureTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASceneCaptureTestGameMode();
};



