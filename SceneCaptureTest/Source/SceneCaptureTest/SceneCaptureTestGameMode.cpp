// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "SceneCaptureTestGameMode.h"
#include "SceneCaptureTestPawn.h"
#include "SceneCaptureTestHud.h"

ASceneCaptureTestGameMode::ASceneCaptureTestGameMode()
{
	DefaultPawnClass = ASceneCaptureTestPawn::StaticClass();
	HUDClass = ASceneCaptureTestHud::StaticClass();
}
