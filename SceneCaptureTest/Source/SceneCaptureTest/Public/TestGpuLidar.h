// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "HAL/ThreadSafeBool.h"
#include "TestGpuLidar.generated.h"


UCLASS( ClassGroup=(Sensors), meta=(BlueprintSpawnableComponent) )
class SCENECAPTURETEST_API UTestGpuLidar : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTestGpuLidar();

	void CaptureScene( uint32 CameraIndex );

	void CaptureSceneDeferred( uint32 CameraIndex );

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void InitializeCameraArray();

	void ReadbackRangePixels( uint32 CameraIndex );

	void ProjectRangePixels(const TArray<float>& RangePixels, int32 Width, int32 Height,
		const FMatrix& ProjectionMatrix,
		TArray<FVector>& Points);

	void DrawRangePoints();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


protected:

	UPROPERTY(EditDefaultsOnly, Category = "Lidar")
		uint32	NumberBeams = 16;

	UPROPERTY(EditDefaultsOnly, Category = "Lidar")
		float	ScanRate = 20.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Lidar")
		float   MinElevationAngle = -15.0;

	UPROPERTY(EditDefaultsOnly, Category = "Lidar")
		float MaxElevationAngle = 15.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Lidar")
		float AzimuthAngleResolution = 0.4f;

	UPROPERTY(EditDefaultsOnly, Category = "Lidar")
		float MaximumRange = 10000.0f;

	UPROPERTY(EditAnywhere, Category = "Sensors")
		bool bEnableSensor = true;

	UPROPERTY(EditAnywhere, Category = "Sensors")
		bool bDrawPoints = true;

	UPROPERTY(EditDefaultsOnly, Category = "Sensors")
		UStaticMesh*	DrawPointMesh = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Sensors")
		float			DrawPointScale = 0.05f; // 5cm assuming 1m shape


protected:

	const uint32 NumCameras = 4;

	TArray<class UReadbackSceneCaptureComponent2D*>		DepthCameraArray;

	FCriticalSection							AccessingDepthBuffer;


	class UInstancedStaticMeshComponent*		InstancedMeshComponent;

	float  CameraFireInterval = 0.10f;
	float  CameraFireAccumulator = 0.0f;
	uint32 NextCameraToFire = 0u;
	
};
