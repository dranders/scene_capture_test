// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneCaptureComponent2D.h"
#include "TestCaptureCamera.generated.h"

/**
 * 
 */
UCLASS( Blueprintable, CLassGroup=(Sensors), meta=(BlueprintSpawnableComponent))
class SCENECAPTURETEST_API UTestCaptureCamera : public USceneCaptureComponent2D
{
	GENERATED_BODY()

public:
  UTestCaptureCamera();

protected:

  virtual void BeginPlay() override;

  void Initialize(uint32 NumBuffers = 2);

  void SetDefaultOverrides();
  void RemoveShowFlags();

  void ReadBackTexture();

public:

  virtual void TickComponent(float DeltaTime, ELevelTick TickType, 
    FActorComponentTickFunction* ThisTickFunction) override;

protected:

  UPROPERTY(EditDefaultsOnly, Category = "Setup")
  uint32 ImageWidth = 512u;

  UPROPERTY(EditDefaultsOnly, Category = "Setup")
  uint32 ImageHeight = 512u;

  UPROPERTY(EditDefaultsOnly, Category = "Setup")
  float FrameRate = 30.0f;

  UPROPERTY(EditAnywhere, Category = "Control")
  bool bEnableCapture = true;

  UPROPERTY(EditAnywhere, Category = "Control")
  bool bEnableReadback = false;

  UPROPERTY(EditDefaultsOnly, Category = "Setup")
  TEnumAsByte<EPixelFormat>		PixelFormat;

private:

  TArray<uint8>       CacheBuffer;

  float CameraDeltaTime = 0.033f;
  float CameraTimeAccumulator = 0.0f;

	
};
