// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "TestLidarRayTrace.generated.h"


UCLASS( Blueprintable, ClassGroup=(Sensors), meta=(BlueprintSpawnableComponent) )
class SCENECAPTURETEST_API UTestLidarRayTrace : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTestLidarRayTrace();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void InitializeLidar();

	void RayTraceLidarBeams();

	void DrawPoints();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Lidar")
		float	ScanRateHz = 10.0f;
	
	UPROPERTY(EditDefaultsOnly, Category = "Lidar")
		uint32	NumberBeams = 16;

	UPROPERTY(EditDefaultsOnly, Category = "Lidar")
		float   MinElevationAngle = -15.0;

	UPROPERTY(EditDefaultsOnly, Category = "Lidar")
		float MaxElevationAngle = 15.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Lidar")
		float AzimuthAngleResolution = 0.4f;

	UPROPERTY(EditDefaultsOnly, Category = "Lidar")
		float MaximumRange = 10000.0f;

	UPROPERTY(EditAnywhere, Category = "Sensors")
		bool bEnableSensor = true;

	UPROPERTY(EditAnywhere, Category = "Sensors")
		bool bDrawPoints = true;

	UPROPERTY(EditDefaultsOnly, Category = "Sensors")
		UStaticMesh*	DrawPointMesh = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Sensors")
		float			DrawPointScale = 0.05f; // 5cm assuming 1m shape

private:

	TArray<FVector>		BeamVectors;

	TArray<float>		HitRanges;
	TArray<FVector>		HitPoints;
	
	TArray<FVector>		WorldPoints;

	class UInstancedStaticMeshComponent*	InstancedMeshComponent;

	float				ScanDeltaTime = 1.0f;
	float				ScanTimeAccumulator = 0.0f;
};
