// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneCaptureComponent2D.h"
#include "ReadbackSceneCaptureComponent2D.generated.h"

/**
 * 
 */
UCLASS()
class SCENECAPTURETEST_API UReadbackSceneCaptureComponent2D : public USceneCaptureComponent2D
{
	GENERATED_BODY()

public:

	struct FCaptureHeader
	{
		FCaptureHeader();

		FTimecode SourceFrameTimecode;
		double    SourceGameTime;
		uint32    SourceFrameNumber;
	};

	struct FCaptureData
	{
		FCaptureData();
		~FCaptureData();

		FCaptureHeader Header;

		void*		  Data;
		uint32		  Width;
		uint32        Height;
		uint32		  PixelDepth;
	};

public:

	UReadbackSceneCaptureComponent2D();

	bool Initialize(int32 Width, int32 Height, EPixelFormat Format, int32 BufferSize = 1);

	void RequestRead();

	bool DataReady() const;

	const FCaptureData& GetCapturedData() const
		{	return CaptureData;		}
	
protected:

	virtual void BeginPlay() override;

	void SetDefaultOverrides();
	void RemoveShowFlags();

protected:

	FCaptureData			CaptureData;
	int32					ImageWidth;
	int32					ImageHeight;
	EPixelFormat			PixelFormat;

	struct FCaptureFrame
	{
		FCaptureFrame();

		FCaptureHeader		Header;

		FTexture2DRHIRef	ReadbackTexture;
		bool				bResolvedTargetRequested;
	};

	FCriticalSection		AccessingRenderTarget;

	TArray<FCaptureFrame>	CaptureBuffer;
	int32					CurrentReadyFrameIndex = 0;
	int32					CurrentResolvedTargetIndex = 0;
	int32					NumberOfCaptureFrames = 0;

	bool                    bResolvedTargetInitialized;
	bool				    bWaitingForResolveCommandExecution;

};
